//
//  userData.swift
//  swiftUITest
//
//  Created by 傅智鹏 on 2021/5/17.
//

import Foundation
import UserNotifications

var encoder = JSONEncoder()
var decoder = JSONDecoder()

let NotificationContent = UNMutableNotificationContent()


class ToDo :ObservableObject{
    @Published var ToDoList:[singleToDo]
    var count = 0
    init() {
        self.ToDoList = []
    }
    init(data:[singleToDo]) {
        self.ToDoList = []
        for item in data {
            self.ToDoList.append(singleToDo(title: item.title, duedate: item.duedate, ischecked: item.ischecked, isFacorite: item.isFacorite, id: self.count))
            count += 1
        }
    }
    func check(id:Int) {
        self.ToDoList[id].ischecked.toggle()
        self.datastore()
    }
        
    
    func add(data:singleToDo){
        self.ToDoList.append(singleToDo(title: data.title, duedate: data.duedate, ischecked: false, isFacorite: data.isFacorite, id: self.count))
        self.count += 1
        self.sort()
        self.datastore()
        self.sendNofitication(id: self.ToDoList.count - 1)
    }
    
    func edit(id:Int,data:singleToDo){
        self.withdrawNofitication(id: id)
        self.ToDoList[id].title = data.title
        self.ToDoList[id].duedate = data.duedate
        self.ToDoList[id].ischecked = false
        self.ToDoList[id].isFacorite = data.isFacorite
        self.sort()
        self.datastore()
        self.sendNofitication(id: id)
    }
    
    func sort() {
        self.ToDoList.sort { data1, data2 in
            return data1.duedate.timeIntervalSince1970 < data2.duedate.timeIntervalSince1970
        }
        for i in 0..<self.ToDoList.count {
            self.ToDoList[i].id = i
        }
        self.datastore()
    }
    
    func delete(id:Int) {
        self.withdrawNofitication(id: id)
        self.ToDoList[id].deleted = true
        self.sort()
        self.datastore()
    }
    
    func datastore (){
        let dataStored = try! encoder.encode(self.ToDoList)
        UserDefaults.standard.setValue( dataStored, forKey: "ToDoList")
    }
    
    func sendNofitication(id:Int) {
        NotificationContent.title = self.ToDoList[id].title
        NotificationContent.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: self.ToDoList[id].duedate.timeIntervalSinceNow, repeats: false)
        let request = UNNotificationRequest(identifier: self.ToDoList[id].title + self.ToDoList[id].duedate.description, content: NotificationContent, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
    
    func withdrawNofitication(id:Int) {//撤回通知
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers:[self.ToDoList[id].title + self.ToDoList[id].duedate.description])
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [self.ToDoList[id].title + self.ToDoList[id].duedate.description])
    }
}

struct singleToDo: Identifiable,Codable{
    var title:String = ""
    var duedate:Date = Date()
    var ischecked:Bool = false
    var deleted:Bool = false
    var isFacorite:Bool = false
    var id:Int = 0
}

//
//  EditingPage.swift
//  swiftUITest
//
//  Created by 傅智鹏 on 2021/5/18.
//

import SwiftUI

struct EditingPage: View {
    @EnvironmentObject var userData:ToDo
    @State var title:String = ""
    @State var duedate:Date = Date()
    @State var isFacorite:Bool = false
    
    
    
    var id:Int? = nil
    
    @Environment(\.presentationMode) var presentation
    var body: some View {
        NavigationView{
            Form{
                Section(header: Text("事项"), content: {
                    TextField("事项内容", text: self.$title)
                    DatePicker(selection: self.$duedate, label: { Text("截止时间") })
                })
                
                Section{
                    Toggle(isOn: self.$isFacorite) {
                        Text("收藏")
                    }
                }
                
                Section{
                    Button(action: {
                        if self.id == nil{//添加
                            self.userData.add(data: singleToDo(title: self.title, duedate: self.duedate,isFacorite: self.isFacorite))
                            self.presentation.wrappedValue.dismiss()
                        }else{//编辑
                            self.userData.edit(id: self.id!, data: singleToDo(title: self.title, duedate: self.duedate,isFacorite: self.isFacorite))
                            self.presentation.wrappedValue.dismiss()
                        }
                    }, label: {
                        Text("确认")
                    })
                    Button(action: {
                        self.presentation.wrappedValue.dismiss()
                    }, label: {
                        Text("取消")
                    })
                }
            }.navigationBarTitle("添加")
            }
        }
}

struct EditingPage_Previews: PreviewProvider {
    static var previews: some View {
        EditingPage()
    }
}
